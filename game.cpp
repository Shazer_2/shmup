#include "game.h"
#include "player.h"
#include "input.h"

Player player = Player(0, 0, 32, 32);

Game::Game() {

}

void Game::render() {
	
}

void Game::update(SDL_Event _event) {
	Input input = Input(_event);

	player.update();
	if (input.keyPressed(SDLK_SPACE)) {
		player.shoot();	
	}
}