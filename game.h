#ifndef GAME_H
#define GAME_H

#include "SDL.h"

class Game {
public:
	Game::Game();
	void Game::render();
	void Game::update(SDL_Event);
};

#endif