#include "input.h"

Input::Input(SDL_Event _event) {
	event = _event;
}

bool Input::eventType(int type) {
	return event.type == type;
}

bool Input::keyPressed(int key) {
	if (eventType(SDL_KEYDOWN)) {
		return event.key.keysym.sym == key;
	}
}

bool Input::keyReleased(int key) {
	if (eventType(SDL_KEYUP)) {
		return event.key.keysym.sym == key;
	}
}