#ifndef INPUT_H
#define INPUT_H

#include "SDL.h"

class Input {
public:
	SDL_Event event;

	Input::Input(SDL_Event);
	Input::Input();
	bool Input::keyPressed(int);
	bool Input::keyReleased(int);
	bool Input::eventType(int);
};

#endif