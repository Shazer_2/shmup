#include "SDL.h"
#include "input.h"
#include "game.h"

#include <iostream>

using namespace std;

SDL_Event event;
SDL_Surface* screen;

bool init() {
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
		return false;
	}

	screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE);
	if (SDL_Flip(screen) == -1) {
		return false;
	}

	SDL_WM_SetCaption("SHMUP", NULL);
	return true;
}

int main(int argc, char* args[]) {
	if (init() == false) {
		return 1;
	}

	bool quit = false;
	Game game = Game();

	while (quit == false) {
		game.render();
		game.update(event);
		while (SDL_PollEvent(&event)) {
			
		}

		SDL_Flip(screen);
	}

	SDL_Quit();
	return 0;
}