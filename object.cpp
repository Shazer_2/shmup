#include "object.h"

Object::Object(int _x, int _y, int _w, int _h) {
	x = _x;
	y = _y;
	w = _w;
	h = _h;
}

void Object::render(SDL_Surface* screen, Uint32 colour) {
	SDL_Rect objectRect = {x, y, w, h};
	SDL_FillRect(screen, &objectRect, colour);
}

void Object::update() {

}