#ifndef OBJECT_H
#define OBJECT_H

#include "SDL.h"

class Object {
public:
	int x, y, w, h;

	explicit Object::Object(int, int, int, int);
	void Object::render(SDL_Surface*, Uint32);
	void Object::update();
};

#endif