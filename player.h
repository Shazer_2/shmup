#ifndef PLAYER_H
#define PLAYER_H

#include "object.h"

class Player: public Object {
public:
	explicit Player::Player(int x, int y, int w, int h) : Object(x, y, w, h) {}
	Player::Player();
	void Player::shoot();
};

#endif